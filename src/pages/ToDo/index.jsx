import { useRef } from 'react';
import { connect, useDispatch } from 'react-redux';

import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';

import uuid from 'react-uuid';

import Stack from '@mui/material/Stack';
import IconButton from '@mui/material/IconButton';
import DarkModeIcon from '@mui/icons-material/DarkMode';
import Paper from '@mui/material/Paper';
import InputBase from '@mui/material/InputBase';
import Checkbox from '@mui/material/Checkbox';
import LightModeIcon from '@mui/icons-material/LightMode';
import CheckBoxIcon from '@mui/icons-material/CheckBox';
import CheckBoxOutlineBlankIcon from '@mui/icons-material/CheckBoxOutlineBlank';

import { setToDos, setTodoClearStatus, setTodoFilter, updateListArrayTodo } from '@containers/Todo/actions';
import { selectTodoFilter, selectTodoList, selectTodoUpdate } from '@containers/Todo/selectors';
import { createTheme, ThemeProvider } from '@mui/material/styles';

import { selectTheme } from '@containers/Theme/selectors';
import { setTheme } from '@containers/Theme/actions';

import { DndContext, closestCenter } from '@dnd-kit/core';
import { arrayMove, SortableContext, verticalListSortingStrategy } from '@dnd-kit/sortable';

import SortableItem from '@components/Todo';
import classes from './style.module.scss';

const STATUS_DONE = 'done';
const STATUS_NOT_DONE = 'not-done';

const ToDo = ({ todos, todoFilter, theme: myTheme, todoUpdate }) => {
  const dispatch = useDispatch();
  const inputRef = useRef(null);
  const checkboxRef = useRef(null);
  const label = { inputProps: { 'aria-label': 'Checkbox' } };

  const handleNewTodo = (e) => {
    const inputValue = inputRef.current.value;
    const { checked } = checkboxRef.current;

    if (e.key === 'Enter' && inputValue.trim() !== '') {
      e.preventDefault();
      const ObjTodo = {
        id: uuid(),
        data: inputValue.trim(),
        status: checked ? STATUS_DONE : STATUS_NOT_DONE,
      };

      dispatch(setToDos(ObjTodo));
      inputRef.current.value = '';
      checkboxRef.current.checked = false;
    }
  };

  // Handle filter selection
  const handleFilter = (selectedFilter) => {
    dispatch(setTodoFilter(selectedFilter));
  };

  const handleClearStatus = () => {
    dispatch(setTodoClearStatus(STATUS_DONE));
  };

  let filteredTodos;
  // Filter todos based on the selected filter
  if (todoFilter === STATUS_DONE) {
    filteredTodos = todos.filter((todo) => todo.status === STATUS_DONE);
  } else if (todoFilter === STATUS_NOT_DONE) {
    filteredTodos = todos.filter((todo) => todo.status !== STATUS_DONE);
  } else {
    filteredTodos = todos;
  }

  const completedCount = todos
    ? todos.filter((todo) => todo.status === STATUS_NOT_DONE).reduce((count) => (count += 1), 0)
    : 0;

  const isLight = myTheme === 'dark';
  const theme = createTheme({
    palette: {
      background: {
        default: isLight ? 'hsl(207, 26%, 17%)' : 'hsl(0, 0%, 98%)',
        paper: isLight ? 'hsl(237, 14%, 26%)' : 'hsl(0, 0%, 98%)',
      },
      text: {
        primary: isLight ? 'hsl(241, 19%, 90%)' : 'hsl(200, 15%, 8%)',
      },
    },
  });

  const handleTheme = () => {
    if (myTheme === 'light') {
      dispatch(setTheme('dark'));
    } else {
      dispatch(setTheme('light'));
    }
  };
  const handleDragEnd = (event) => {
    const { active, over } = event;

    if (active.id !== over.id) {
      const activeIndex = todos.findIndex((item) => item.id === active.id);
      const overIndex = todos.findIndex((item) => item.id === over.id);

      dispatch(updateListArrayTodo(arrayMove(todos, activeIndex, overIndex)));
    }
  };
  return (
    <ThemeProvider theme={theme}>
      <DndContext collisionDetection={closestCenter} onDragEnd={handleDragEnd}>
        <div className={`${classes.todo} ${isLight ? classes.dark : classes.light}`}>
          <div className={classes.container}>
            <Stack direction="column" spacing={3}>
              <div className={classes.content_title}>
                <div className={classes.text_title}>TODO</div>
                <IconButton aria-label="dark mode" onClick={handleTheme}>
                  {isLight ? <LightModeIcon sx={{ color: 'white' }} /> : <DarkModeIcon />}
                </IconButton>
              </div>
              <Paper component="form">
                <Checkbox
                  {...label}
                  icon={<CheckBoxOutlineBlankIcon />}
                  checkedIcon={<CheckBoxIcon />}
                  onChange={(e) => handleNewTodo(e)}
                  inputRef={checkboxRef}
                  disabled
                />
                <InputBase
                  sx={{ ml: 1, flex: 1 }}
                  placeholder="..."
                  inputProps={{ 'aria-label': 'todo list' }}
                  onKeyDown={handleNewTodo}
                  inputRef={inputRef}
                />
              </Paper>
              <Paper component="form" style={{ maxHeight: 400, overflow: 'auto' }}>
                <Stack direction="column">
                  <SortableContext items={filteredTodos} strategy={verticalListSortingStrategy}>
                    {filteredTodos &&
                      filteredTodos.map((val, i) => (
                        <div className={classes.underline} key={i}>
                          <SortableItem val={val} todoUpdate={todoUpdate} classes={classes} />
                        </div>
                      ))}
                  </SortableContext>
                </Stack>
              </Paper>
            </Stack>
            <Paper sx={{ p: '10px' }}>
              <Stack direction="row" spacing={3} justifyContent="space-between" alignItems="center">
                <div className={classes.sub_text}>{completedCount} item left</div>
                <Stack direction="row" spacing={2}>
                  <div
                    className={`${classes.sub_text} ${todoFilter === 'all' ? classes.active : ''}`}
                    onClick={() => handleFilter('all')}
                    style={{ cursor: 'pointer' }}
                  >
                    All
                  </div>
                  <div
                    className={`${classes.sub_text} ${todoFilter === 'not-done' ? classes.active : ''}`}
                    onClick={() => handleFilter(STATUS_NOT_DONE)}
                    style={{ cursor: 'pointer' }}
                  >
                    Active
                  </div>
                  <div
                    className={`${classes.sub_text} ${todoFilter === 'done' ? classes.active : ''}`}
                    onClick={() => handleFilter(STATUS_DONE)}
                    style={{ cursor: 'pointer' }}
                  >
                    Completed
                  </div>
                </Stack>
                <div className={classes.sub_text} style={{ cursor: 'pointer' }} onClick={handleClearStatus}>
                  Clear Completed
                </div>
              </Stack>
            </Paper>
          </div>
        </div>
      </DndContext>
    </ThemeProvider>
  );
};

ToDo.propTypes = {
  todos: PropTypes.array,
  todoFilter: PropTypes.string,
  theme: PropTypes.string,
  todoUpdate: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  todos: selectTodoList,
  todoFilter: selectTodoFilter,
  theme: selectTheme,
  todoUpdate: selectTodoUpdate,
});

export default connect(mapStateToProps)(ToDo);
