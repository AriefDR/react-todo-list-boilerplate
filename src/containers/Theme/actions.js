import { SET_THEME } from '@containers/Theme/constants';

export const setTheme = (theme) => ({
  type: SET_THEME,
  theme,
});
